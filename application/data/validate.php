<?php 

class Validate{
	
$error = array(
	'postname'=>'',
	'posttitle'=>'',
	'postbody'=>''
	);

function validatePosts($_POST){

		if( !empty($_POST)){
				$form_valid = true;
			
				$_POST['name'] = trim($_POST['name']);//trim
				$_POST['title'] = trim($_POST['title']);//trim
				$_POST['body'] = $_POST['body'];

			            
					if ($_POST['name'] =='' || (!isset($_POST['name']))){//if value is blank or if value is not set.
					$error['postname'] = 'Name is required';//echo the error message
					$form_valid =false;//change $form_valid to false.
					
					}
				
					if ($_POST['title'] =='' ){/*if blank*/
					$error['posttitle'] = 'Title is required';
					$form_valid =false;
					
					}
				
					if ($_POST['body'] =='' ){
					$error['postbody'] = 'Please write a comment';
					$form_valid =false;
					
					}
				
			return $form_valid;
				
		}
}