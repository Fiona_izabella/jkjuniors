<?php 

class Validate_m {

	public $form_valid;
	public $newsForm_valid;
	public $resultsArray = array();

	public $error = array(
		'postname'=>'',
		'posttitle'=>'',
		'postbody'=>''
		);

	function validatePosts($name, $title, $body){

		$error = array(
			'postname'=>'',
			'posttitle'=>'',
			'postbody'=>''
			);

		if ($name =='' || (!isset($name))){
			$error['postname'] = 'Name is required';
			$this->form_valid =false;

		}

		if ($title =='' ){/*if blank*/
			$error['posttitle'] = 'Title is required';
			$this->form_valid =false;
		}

		if ($body =='' ){
			$error['postbody'] = 'Please write a comment';
			$this->form_valid =false;
		} else {

			$this->form_valid = true;
		}

		return $error;

	}

	/*====for news blog class=====*/

	function validateNews($title, $body, $created_by){

		$error = array(
			'posttitle'=>'',
			'postbody'=>'',
			'postcreated_by'=>''
			);

		$newsForm_valid = "true";
		$resultsArray = array();
		if ($title =='' || (!isset($title))){
			$error['posttitle'] = 'title is required';
			$newsForm_valid ="false";

		}

		if ($body =='' ){/*if blank*/
			$error['postbody'] = 'body is required';
			$newsForm_valid ="false";

		}

		if ($created_by =='' ){
			$error['postcreated_by'] = 'your name is required';
			$newsForm_valid ="false";

		} 


		$resultsArray['boolValue'] =  $newsForm_valid;
		$resultsArray['errorValue'] =  $error;
		return $resultsArray;

	}

	function get_newsForm_valid(){
		$newsForm_valid = $this->newsForm_valid;
		return $newsForm_valid;
	}
}