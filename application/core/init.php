<?php 
#starting the users session
session_start();
require 'connect/database.php';
require 'classes/users.php';
require 'classes/general.php';
 
/** 
 * When we include this init.php in our files in the root directory, those files will have 3 objects already created in them, which are $general, $users and $db. We created $general and $users in this file by instantiating General class and Users class that are present in general.php and users.php; whereas, $db was already created in our database.php.
 * Now we will be able to use all the functions that we will create in our General and Users class and the built-in functions in the PDO class in all our pages.
 */

$users 		= new Users($db);
$general 	= new General();
 
$errors 	= array();