<div class="main-container clearfix">
    <div class="jumbotronContact">
        <span>If you have a query then contact jk juniors on our contact form, give us a call, or just pop in!</span>
        <style type="text/css" >
            .errorMsg{border:1px solid red; }
            .message{padding: 10px; color: red; font-weight:bold; }
        </style>
    </div> 

    <div class="main wrapper clearfix">
        <article class="contact">
            <div id="starContainer" class="rotate"></div>
            <header>
                <h1 class="overLayTitle">Contact form</h1>
            </header>
            <form class="contact" method="post" action="">
                <?php if (isset($errorMsg)) { echo "<p class='message'>" .$errorMsg. "</p>" ;} ?>

                <?php echo "<h4>$status</h4>"; ?>
                <label>Name *</label>
                <input type="text" name="name" placeholder = "name here" 
                value="<?php if(isset($name)){echo $name;} ?>" required />
                <?php if(isset($code) && $code == 1){
                    echo "$errorMsg" ;
                } ?> 

                <label>Tel number *</label>
                <input name="telNumber" type="number" placeholder="Type Here" 
                value="<?php if(isset($number)){echo $number;} ?>"
                required />
                <?php if(isset($code) && $code == 2){echo "errorMsg" ;}?> 
                <label>Email *</label>
                <input name="email" type="email" placeholder="Type Here" value="
                <?php if(isset($email)){echo $email;} ?>" required />
                <?php if(isset($code) && $code == 3){echo "class=errorMsg" ;}?> 
                <label>Message *</label>
                <textarea name="message" placeholder="Type Here"  
                value="<?php if(isset($message)){echo $message;} ?>" required /></textarea>
                <?php if(isset($code) && $code == 4){echo "errorMsg" ;}?> 
                <input id="submit" name="submit" type="submit" value="Submit">
                <div class="clearfix"></div>
            </form>
            <section>
                <div id="starContainer2" class="rotate"></div>
                <h2>Our address</h2>
            </section>

            <footer class = "footerAddress">
                <p>49 Hemdean road, Caversham Berkshire RG4 7SS</p>
                <p class="number">0118 9477 340</p>
            </footer>
        </article>

        <aside>
            <h3>Location</h3>
            <h5>Jk Juniors Cuts</h5>
            <p>49 HEMDEAN ROAD CAVERSHAM</p>
            <div class="map">
                <iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=49+HEMDEAN+ROAD+CAVERSHAM&amp;aq=&amp;sll=51.519725,-0.979385&amp;sspn=0.006369,0.015857&amp;ie=UTF8&amp;hq=&amp;hnear=49+Hemdean+Rd,+Reading+RG4+7SS,+United+Kingdom&amp;t=m&amp;ll=51.470905,-0.973406&amp;spn=0.016039,0.025749&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.uk/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=49+HEMDEAN+ROAD+CAVERSHAM&amp;aq=&amp;sll=51.519725,-0.979385&amp;sspn=0.006369,0.015857&amp;ie=UTF8&amp;hq=&amp;hnear=49+Hemdean+Rd,+Reading+RG4+7SS,+United+Kingdom&amp;t=m&amp;ll=51.470905,-0.973406&amp;spn=0.016039,0.025749&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small>
            </div>
            <p>We had a great opening on the 29th April! The salon was packed and we had our favourite cartoon characters
                out on the streets handing out leaflets!</p>
                <h3>Directions</h3>
                <p>We are located just past the Caversham library on Hemdean road.</p>
                <h3>Buses</h3>
                <p>Bus numbers 22 and 24 come past the salon.</p>
                <h3>Car parking</h3>
                <p>Weekends: You can park your car in the surgery around the corner.<br /><img src="assets/img/car_small.png" class="left"> Weekdays: 2-hour car parking around the corner, there is also a car park behind The Baron Cadogan and behind Waitrose.</p>

                <div id="wrap">
                    <div class="bubble_wrap">
                        <div class="bubbles b0"></div>
                        <div class="bubbles b1"></div>
                        <div class="bubbles b2"></div>
                        <div class="bubbles b3"></div>
                        <div class="bubbles b4"></div>
                        <div class="bubbles b5"></div>
                        <div class="bubbles b6"></div>
                        <div class="bubbles b7"></div>
                    </div>
                    <div class="rock_5"></div> 
                </div>
                <!--for adding facebook comment-->
                <div class="fb-like-box" data-href="https://www.facebook.com/jolanta.kilroy?fref=ts" data-width="200" data-height="200" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>
        </aside>    
        <div class="table"></div>
    </div> <!-- #main -->
</div> <!-- #main-container -->