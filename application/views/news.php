<div class="main-container clearfix">
    <div class="jumbotron">
        <div class="slides">
            <img src="assets/slideShow/pic1.jpg" />
            <img src="assets/slideShow/pic2.jpg" />
            <img src="assets/slideShow/pic3.jpg" />
            <img src="assets/slideShow/pic4.jpg" />
            <img src="assets/slideShow/pic5.jpg" />
            <img src="assets/slideShow/pic6.jpg" />
            <img src="assets/slideShow/pic7.jpg" />
        </div>
        <canvas id="slideshow" width="900" height="300"></canvas>
        <div class="slideShowPhrase">
            <h6 class="jumbotronLabels">Jk Juniors</h6></div>
        <div class="slideShowPhrase1">
            <h6 class="jumbotronLabels2">Now OPEN!</h6>
        </div>
    </div>
    <div class="main wrapper clearfix">
        <?php
        $pag = new PaginationComments();
        $numbers = $pag->Paginate( $printedPosts,5 );
        $r= $pag->fetchResult();
        ?>
            <ul class="paginate">
                <?php
          foreach ($numbers as $n) {
            echo '<li><a href="comments&page='.$n.'">'.$n.'</a></li>';
          }
          ?>
            </ul>
            <article class="news">
                <div id="starContainer" class="rotate"></div>
                <header>
                    <h1 class="overLayTitle">Comments</h1>
                </header>
                <?php foreach ($r as $newRecord) :?>
                <? $timestamp = strtotime($newRecord['created_at']);?>
                    <ul class="comments">
                        <li class="commentsTitle">
                            <?php echo $newRecord['title']?>
                        </li>
                        <li class="message">
                            <?php echo $newRecord['body']?>
                        </li>
                        <li class="commentsDate">date:
                            <?php echo date('d/m/Y', $timestamp) ?>
                        </li>
                        <li class="commentsFrom">From:
                            <?php echo $newRecord['name']?>
                        </li>
                        <hr />
                    </ul>
                    <?php endforeach;?>
                    <section>
                        <div id="starContainer2" class="rotate"></div>
                    </section>
                    <footer>
                        <h3>We appreciate your comments</h3>
                    </footer>
            </article>
            <aside>
                <header>
                    <h3>Add a comment</h3>
                </header>
                <form method="post" action="" class="contact">
                    <div class="red">
                        <p>
                            <?php echo $error['postname']; //error message for validation ?>
                        </p>
                        <p>
                            <?php echo $error['posttitle']; //error message for validation ?>
                        </p>
                        <p>
                            <?php echo $error['postbody']; //error message for validation ?>
                        </p>
                    </div>
                    <label>Title</label>
                    <input type='text' placeholder='Title' name='title' value='<?php echo $data->form_last_value(' title '); ?>' required/>
                    <label>Name</label>
                    <input type='text' placeholder='Name' name='name' value='<?php echo $data->form_last_value(' name '); ?>' required/>
                    <label>Comments</label>
                    <textarea name='body' placeholder="Add your comments here" value="<?php echo $data->form_last_value('body'); ?>" required /></textarea>
                    <p>
                        <input type="submit" value='submit' id='submit' />
                    </p>
                    <div class="clearfix"></div>
                </form>
                <div class="fbookSection">
                    <h3>Facebook news</h3>
                    <!--for adding facebook comment-->
                    <div class="fb-comments" data-href="http://example.com/fiona.sibilski" data-width="200px" data-numposts="5" data-colorscheme="light"></div>
                </div>
            </aside>
    </div>
    <div class="table"></div>
</div>
