<div class="main-container clearfix">
    <div class="jumbotron-container">
        <div class="jumbotron">
            <div class="slides">
                <img src="assets/slideShow/pic1.jpg" />
                <img src="assets/slideShow/pic2.jpg" />
                <img src="assets/slideShow/pic3.jpg" />
                <img src="assets/slideShow/pic4.jpg" />
                <img src="assets/slideShow/pic5.jpg" />
                <img src="assets/slideShow/pic6.jpg" />
                <img src="assets/slideShow/pic7.jpg" />
                <img src="assets/slideShow/pic8.png" />
                <img src="assets/slideShow/pic9.png" />
                <img src="assets/slideShow/pic10.png" />
                <img src="assets/slideShow/pic11.png" />
            </div>
            <canvas id="slideshow" width="900" height="300"></canvas>
            <div class="slideShowPhrase">
                <h6 class="jumbotronLabels">Jk Juniors cuts</h6>
            </div>
        </div>
    </div>
    <div class="main wrapper clearfix">
        <article>
            <div id="starContainer" class="rotate"></div>
            <header>
                <h1 class="overLayTitle">JK juniors cuts</h1>
                <p>Welcome to our new amazing kids hair salon in Caversham. We offer quality hair cuts for kids and parents in a bright, inviting and child friendly environment. </p>
                <p>Sit back and relax with a coffee whilst our play area entertains your children and our trained stylists look after your childrens hair. Come in and say hello..</p>
            </header>
            <section>
                <div id="starContainer2" class="rotate"></div>
                <h2>Making hair fun</h2>
                <p>We have car chairs, tons of toys, stickers, lollies and sweets and a play area for children with all the current favourite Dvd's.</p>
                <p>Once kids get onto our special car chairs they dont want to get off!</p>
            </section>
            <footer class="footerAddress">
                <p>49 Hemdean road, Caversham Berkshire RG4 7SS</p>
                <p class="number">0118 9477 340</p>
            </footer>
        </article>
        <aside>
            <h3>jk juniors opening</h3>
            <p>We had a great opening on the 29th April! The salon was packed and we had our favourite cartoon characters out on the streets handing out leaflets!</p>
            <h3>jk juniors news</h3>
            <p>We have acquired even more toys and movies!..and we have a life-size mickey mouse character. We welcome everyone to have a photo with mickey.
            </p>
            <h3>news</h3>
            <p>As you may hae noticed we have changed our name from Kidz kutz to jk juniors cuts!</p>
            <p>This week we are showing lots of movies and have some special offers...<img src="assets/img/car_small.png" class="left">Come for a visit and ask our friendly staff about any current offers available. </p>
            <div id="wrap">
                <div class="bubble_wrap">
                    <div class="bubbles b0"></div>
                    <div class="bubbles b1"></div>
                    <div class="bubbles b2"></div>
                    <div class="bubbles b3"></div>
                    <div class="bubbles b4"></div>
                    <div class="bubbles b5"></div>
                    <div class="bubbles b6"></div>
                    <div class="bubbles b7"></div>
                </div>
                <div class="rock_5"></div>
            </div>
            <!--for adding facebook comment-->
            <div class="fb-like-box" data-href="https://www.facebook.com/jolanta.kilroy?fref=ts" data-width="200" data-height="200" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>
        </aside>
    </div>
</div>
