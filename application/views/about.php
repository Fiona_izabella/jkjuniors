<div class="main-container clearfix">

  <div class="jumbotron">
    <div class="slides">
      <img src="assets/slideShow/pic1.jpg" />
      <img src="assets/slideShow/pic2.jpg" />
      <img src="assets/slideShow/pic3.jpg" />
      <img src="assets/slideShow/pic4.jpg" />
      <img src="assets/slideShow/pic5.jpg" />
      <img src="assets/slideShow/pic6.jpg" />
      <img src="assets/slideShow/pic7.jpg" /> 
    </div>
    <canvas id="slideshow" width="900" height="300"></canvas>

    <div class="slideShowPhrase">
      <h6 class="jumbotronLabels">JK Juniors</h6>
      </div>
      <div class="slideShowPhrase1">
        <h6 class="jumbotronLabels2">Now OPEN!</h6>
      </div>
    </div>

    <div class="main-news wrapper clearfix">
      <aside class="news">
        <h1 class="text-centred">JK Juniors news</h1>
        <?php 
        $news = $blog_widget->outputblog(); 
        ?>
        <ul class="comments block">
          <?php foreach ($news as $n) :?>
            <li class="commentsTitle">Title: <?php echo $n['title']?></li>
            <li class="message"><?php echo $n['body']?></li>
            <?php if($n['image_src'] != ""): ?>
              <img src="assets/uploadedImages/<?php echo $n['image_src']?>" class="centered ">
            <?php endif;?>
            <? $timestamp = strtotime($n['created_at']);?>
            <li class="commentsDate">Date: <?php echo date('d/m/Y', $timestamp) ?></li>
            <li class="commentsFrom">From: <?php echo $n['created_by']?></li>
            <hr>
          <?php endforeach; ?>
        </ul>
      </aside>
    </div>
  </div>

