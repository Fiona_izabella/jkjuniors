<div class="main-container clearfix">

  <div class="main wrapper clearfix">
    <article>
      <div id="starContainer" class="rotate"></div>
      <header>
        <h1 class="overLayTitle">Add news</h1>               
      </header>
      <section>
        <p>Fill in teh form with your latest news</p>
        <h2>News form</h2>
        <?php $blog_widget -> Newsblog(); ?>            
      </section>

      <div id = "imageForm">
        <h2>Fill in the form to submit an image</h2>
        <hr/>
        <?php echo " $complete" ?>
        <?php echo "$imageErrors"?>
        <form method="post" action="" id="uploadForm" enctype="multipart/form-data" >
          <!--form for uploading an image-->
          <p><label for="upload">upload</label><span class='red'><?php echo "error here"//echo '  '.$error_image;  ?></span></p>
          <p>select a post id: </p>
          <select name = "post_idNumber">
            <?php  $numb = $blog_widget -> outputblog();
            foreach ($numb as $n) {
              echo '<option value = ' . $n['id'] . '>' . $n['id'] . ' </option>';
            }
            ?>
          </select>
          <p><input type="file" name="my_file" /></p>
          <p><input type="submit" id="uploadImage" value="upload image" / ></p>
          <p><input type="hidden" name="action" value="upload"  id="submit"/></p>
        </form>		
        <div class="clearfix"></div>
      </div><!--end of image form-->
    </article>
    <aside>
      <h2>News preview</h2>
      <?php $news = $blog_widget -> outputblog(); ?>
      <ul>
        <?php foreach ($news as $n) :?>
          <li>post Id: <?php echo $n['id']?></li>
          <li>title: <?php echo $n['title']?></li>
          <li>date: <?php echo $n['created_at']?></li>
          <li>body: <?php echo $n['body']?></li>
          <li>title: <?php echo $n['created_by']?></li>
          <li>image file: <?php echo $n['image_src']?></li>
          <hr>
        <?php endforeach; ?>
      </ul>
    </aside>    
  </div>
  <div class="table"></div>
</div>