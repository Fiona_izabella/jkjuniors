<div class="header-container clearfix">
    <nav>
        <ul class="menu-container">
            <li>
                <div class="logo"></div>
                <a class="clicker"><span>Menu</span></a>
                <ul class="navList">
                    <li>
                        <a href="home">home</a>
                    </li>
                    <li>
                        <a href="news">news</a>
                    </li>
                    <li>
                        <a href="services">services</a>
                    </li>
                    <li>
                        <a href="comments">comments</a>
                    </li>
                    <li>
                        <a href="gallery">pics</a>
                    </li>
                    <li>
                        <a href="contact">contact</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>
