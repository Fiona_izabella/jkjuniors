<div class="main-container clearfix">

  <div class="jumbotron">
    <div class="slides">
      <img src="assets/slideShow/pic1.jpg" alt="Image of kidzkutz salon" />
      <img src="assets/slideShow/pic2.jpg" alt="Image of kidzkutz salon" />
      <img src="assets/slideShow/pic3.jpg" alt="Image of kidzkutz salon" />
      <img src="assets/slideShow/pic4.jpg" alt="Image of kidzkutz salon" />
      <img src="assets/slideShow/pic5.jpg" alt="Image of kidzkutz salon" />
      <img src="assets/slideShow/pic6.jpg" alt="Image of kidzkutz salon" />
      <img src="assets/slideShow/pic7.jpg" alt="Image of kidzkutz salon" /> 
      <img src="assets/slideShow/pic8.png" alt="Image of kidzkutz salon" /> 
      <img src="assets/slideShow/pic9.png" alt="Image of kidzkutz salon" /> 
      <img src="assets/slideShow/pic10.png" alt="Image of kidzkutz salon" /> 
      <img src="assets/slideShow/pic11.png" alt="Image of kidzkutz logo" /> 
    </div>
    <canvas id="slideshow" width="900" height="300"></canvas>
    <div class="slideShowPhrase">
      <h6 class="jumbotronLabels">
        JK juniors</h6></div>
        <div class="slideShowPhrase1">
          <h6 class="jumbotronLabels2">now OPEN!</h6>
        </div>
      </div>
      <div class="main wrapper clearfix">
        <article class="prices">
          <div id="starContainer" class="rotate"></div>
          <header>
            <h1 class="overLayTitle">JK Juniors Services</h1>
            <p class="shortenParagraph">
              Our stylists offer hair cuts for children of all ages. Dads and mums can 
              also get a haircut and enjoy the family experience!
              Our car seats make having your hair cut an enjoyable experience for your children 
              and our play area and dvds are guaranteed to keep everyone amused!
            </p>
          </header>
          <section>
            <div id="starContainer2" class="rotate"></div>
            <h2>Opening hours</h2>
            <ul class="prices white-bg">
              <li>Monday: 9.30am - 5.30pm</li>
              <li>Tuesday: Closed</li>
              <li>Wednesday: 9.30am - 5.30pm </li>
              <li>Thursday: 12pm - 5.30pm</li>
              <li>Friday: 9.30am - 5.30pm</li>
              <li>Saturday: 9.30am - 5.30pm</li>
            </ul>
            <p class="shortenParagraphLabel">No appointments are necessary, but feel free to give us a call if you
              have any queries.</p>
            </section>

            <footer>
            </footer>
          </article>
          <aside>
            <h3>services</h3>
            <ul class="prices">
              <li>fringe cuts</li>
              <li>wet cuts</li>
              <li>cut and blow drys</li>
              <li>hair plaiting</li>
              <li>kids all ages 0-18</li>
              <li>Dads cuts</li>
              <li>mums cuts</li>
            </ul>
            <h3>extras</h3>
            <ul class="prices">
              <li>play area</li>
              <li>baby changing facilities</li>
              <li>toys</li>
              <li>lollies</li>
              <li>childrens dvd's</li>
              <li>colourful cars seats</li>
              <li>refreshments for parents</li>
              <li>certificates for first hair cuts</li>
            </ul>

            <div id="wrap">

              <div class="bubble_wrap">
                <div class="bubbles b0"></div>
                <div class="bubbles b1"></div>
                <div class="bubbles b2"></div>
                <div class="bubbles b3"></div>
                <div class="bubbles b4"></div>
                <div class="bubbles b5"></div>
                <div class="bubbles b6"></div>
                <div class="bubbles b7"></div>
              </div>
              <div class="rock_5"></div> 

            </div>

            <!--for adding facebook comment-->

            <div class="fb-like-box" data-href="https://www.facebook.com/jolanta.kilroy?fref=ts" data-width="200" data-height="200" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>

          </aside>    

        </div>
        <div class="table"></div>

      </div>
