<div class="footer-container clearfix">
    <footer class="wrapper">
        <ul>
            <li><a href="http://fionawebdesign.co.uk/">&copy; fionawebdesign</a></li>
            <li><a href="http://www.kilroyhair.co.uk">&copy; kilroy hair</a></li>
            <li><a href="contact"><img src="assets/img/emailIcon.png" target="blank" alt="icon image link to contact page"></a></li>
            <li><a href="https://www.facebook.com/kidzkutzcaversham" target="blank" alt="icon image link to  facebook page"><img src="assets/img/facebook.png"></a></li>
        </ul>
    </footer>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/pixastic.custom.js"></script>
<script src="assets/js/script.js"></script>
<script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
        g.src='//www.google-analytics.com/ga.js';
        s.parentNode.insertBefore(g,s)}(document,'script'));
</script>


</body>
</html>
