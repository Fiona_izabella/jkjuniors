<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Specialising in kid’s haircuts - jk juniors cuts</title>
    <meta name="description" content="Hair salon located in Caversham specialising in kid’s haircuts. 
    Child-friendly environment with kids entertainment and crèche making the experience better for both parents and children.  
    Parents can relax with a coffee whilst trained stylists cut their children's hair. Going to the hairdressers can be fun!">
    <meta name="keywords" content="hair cuts for kids, Hair salon for kids in Caversham, jk juniors cuts in Caversham, jk juniors Caversham, Children's stylists, 
    child-friendly environment, kids entertainment, affordable childrens hait cuts, Specialising in kid’s haircuts." />
    <meta name="author" content="fiona przybylski" />
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="assets/css/normalize.min.css">
    <link rel="stylesheet" href="assets/css/myStyles.css">
    <link rel="stylesheet" href="assets/css/bubble.css">
    <link rel="stylesheet" href="assets/css/pagination.css">

    <script src="assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="assets/js/kinetic-v5.1.0.min.js"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<!--for adding facebook comment-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



