<?php
class Contact {

	public function __construct() {
	}

	public function contact() {
		$status ='Please fill in our form and we will get back to you';
		if(!empty($_POST)){

			$name = trim($_POST['name']);
			$email = filter_var(
				trim($_POST['email']),
				FILTER_SANITIZE_EMAIL
				);
			$message = $_POST['message'];
			$number = $_POST['telNumber'];
			if($name =="") {
				$errorMsg=  "error : You did not enter a name.";
				$code= "1" ;
			}
			elseif($number == "") {
				$errorMsg=  "error : Please enter number.";
				$code= "2";
			}
			elseif(is_numeric(trim($number)) == false){
				$errorMsg=  "error : Please enter numeric value.";
				$code= "2";
			}
			elseif(strlen($number)<10){
				$errorMsg=  "error : Number should be ten digits.";
				$code= "2";
			}
			elseif($email == ""){
				$errorMsg=  "error : You did not enter a email.";
				$code= "3";
			}
			elseif(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)){
				$errorMsg= 'error : You did not enter a valid email.';
				$code= "3";
			}

			elseif($message =="") {
				$errorMsg=  "error : You did not enter a message";
				$code= "4" ;
			}

			else{
				$to = 'fionka78@googlemail.com';
				$subject = 'kidzkutz enquiry';
				$body = "you recieved an email from $name \n";
				$body.="There email is $email \n"; 
				$body.="There message is.... $message \n";
				$result = mail($to,$subject,$body);

				if($result){

					$status = "Thank you for your message $name we will get back to you as soon as possible.";
				} else{

					$status = "Sorry $name but your message couldnt send.";
				}
			}

		}

		include (VIEWS . 'header.php');
		include (VIEWS . 'nav.php');
		include (VIEWS . 'contact.php');
		include (VIEWS . 'footer.php');

	}

}
