<?php
class Uploading {

	public $upload_limit = 1;
	public $complete;
	public $error_image;

	public function __construct() {
		include ('application/data/upload_m.php')
	}

	function uploading() {
		$file = "";
		$uploadfolder = "assets/uploadedImages/";
		$action = "";
		$error = array(//create an associative empty array that you fill up.
		'username' => '', 'description' => '', 'price' => '', 'keyword' => '');


		if (!empty($_FILES) && isset($_FILES['my_file']) && !empty($_POST) && isset($_POST)) {
			$form_valid = true;
			$action = $_POST['action'];

			if ($this->upload_limit > 1) {
				$this->complete = "!! sorry you have exceeded your upload limit";

			} else if ($_FILES['my_file']['size'] > 1000000) {/*if file size is greater than 1000000*/
				$this->complete = "!! sorry image too large";

			} elseif ($this->upload_limit < 2) {
				/*store $_FILES values in variables*/

				$image_name = $_FILES['my_file']['name'];
				$image_type = $_FILES['my_file']['type'];
				$image_error = $_FILES['my_file']['error'];
				$image_size = $_FILES['my_file']['size'];
				$image_size_conv = floor($_FILES['my_file']['size'] / 1024);
				/*convert to kb*/
				$post_id = $_POST['post_idNumber'];

				switch($_FILES['my_file']['error']) {
					case UPLOAD_ERR_OK :
					if  (($_FILES['my_file']['type'] == 'image/jpeg') 
						|| ($_FILES["my_file"]["type"] == "image/png"))

					{
						$file = $_FILES['my_file']['tmp_name'];
						$destination = $uploadfolder . $_FILES['my_file']['name'];
						$result = move_uploaded_file($file, $destination);

					} else {
						$this->complete = "wrong type file";
						$form_valid = false;

					}
					break;
					case UPLOAD_ERR_INI_SIZE :
					$this->complete = "size too large";
					$form_valid = false;
					break;

					case UPLOAD_ERR_FORM_SIZE :
					$this->complete = "file size too large";
					$form_valid = false;
					break;

					case UPLOAD_ERR_PARTIAL :
					$this->complete = "upload error";
					$form_valid = false;
					break;

					case UPLOAD_ERR_NO_FILE :
					$this->error_image = "upload error";
					$form_valid = false;
					$this->complete = "please select an image to upload";
					break;
				}

				if ($form_valid == true) {
					$uploadModel = new upload_m();
					$success = $uploadModel->insertImageInfo($image_name, $post_id);
					$write = '';
					$this->complete = "<p>Congratualtions .Your image has uploaded.</p>

					<p>Image title: $image_name. </p><p>Image size: $image_size_conv Kb.</p>
					<p>Stay on page to submit another image.</p>";
				}
			}
		}
	}

	function getComplete(){
		return $this->complete;
	}

	function getErrors(){
		return $this->error_image;

	}

}