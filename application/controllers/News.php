<?php
class News {

	public function __construct() {

		include('application/controllers/newsblog.php');

	}

	public function news() {

		$blog_widget = new newsblog();

		include (VIEWS . 'header.php');
		include (VIEWS . 'nav.php');
		include (VIEWS . 'about.php');
		include (VIEWS . 'footer.php');
	}
}


