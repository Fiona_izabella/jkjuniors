<?php
class Home {

	public function __construct() {

	}

	public function home() {

		include (VIEWS . 'header.php');
		include (VIEWS . 'nav.php');
		include (VIEWS . 'home.php');
		include (VIEWS . 'footer.php');

	}

}
