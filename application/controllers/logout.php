<?php
class Logout {

	public function __construct() {

	}

	public function logout() {
		session_start();
		session_destroy();
		header('Location:news');

	}
}
