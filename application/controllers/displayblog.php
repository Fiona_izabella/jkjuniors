<?php

class Displayblog {

  public function __construct(){
    include('application/data/blog_m.php');
  }

  public function Displayblog() {

    $blogModel = new blog_m();
    $count = $blogModel->commentCount();
    $allNewsPosts = $blogModel->printNews();

    if( !empty($_POST)){
      $title = trim($_POST['title']);//trim
      $body = $_POST['body'];
      $created_by = trim($_POST['created_by']);//trim
      $blogModel->insertData($title, $body, $created_by);

      $error = array(
        'postcreated_by'=>'',
        'posttitle'=>'',
        'postbody'=>''
        );
      echo "form sent";

      }

      $blogModel->addFormView('newsblog.php');
  } 

}