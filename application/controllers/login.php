<?php
class Login {

	public function __construct() {
	}

	public function login() {

		require 'application/core/init.php';
		if (empty($_POST) === false) {

			$username = trim($_POST['username']);
			$password = trim($_POST['password']);

			if (empty($username) === true || empty($password) === true) {
				$errors[] = 'Sorry, but we need your username and password.';
			} else if ($users->user_exists($username) === false) {
				$errors[] = 'Sorry that username doesn\'t exists.';
			} 
			else {
				$login = $users->login($username, $password);
				if ($login === false) {
					$errors[] = 'Sorry, that username/password is invalid';
				}else {

				$_SESSION['id'] =  $login; // The user's id is now set into the user's session  in the form of $_SESSION['id'] 
				header('Location: adminNews');
				exit();
			}
		}
	} 

	include (VIEWS . 'login.php');
	}

}


