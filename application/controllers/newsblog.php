<?php
class Newsblog {

    public $allNewsPosts = array();
    public $validTest;
    public $valueOfFormValid;
    public  $title;
    public $error = array(
        'postcreated_by'=>'',
        'posttitle'=>'',
        'postbody'=>''
        );

    public function __construct(){

        include('application/data/blog_m.php');
        include('application/data/validate_m.php');
        $this->blogModel = new blog_m();
        $this->blogModel2 = new blog_m();
        $this->valid = new validate_m();
    }

    public function Newsblog() {
        $count = $this->blogModel->commentCount();
        $this->validTest = $this->valid->newsForm_valid;
        $this->blogModel->addFormView('newsblog.php', $this->error, $this->blogModel2);
    }

    public function insert(){

    $title = trim($_POST['title']);//trim
    $body = $_POST['body'];
    $created_by = trim($_POST['created_by']);//trim
    $test = $this->valid->validateNews($title, $body, $created_by );

        if( $test['boolValue'] == "true"){


            $this->blogModel->insertData($title, $body, $created_by);

            $error = array(
                'postcreated_by'=>'',
                'posttitle'=>'',
                'postbody'=>''
                );
            echo "form sent";

        }   else if ($test['boolValue'] == "false") {

            echo "form failed";
            $this->error = $test['errorValue'];
        }
    }

    public function outputblog(){
        $count = $this->blogModel2->commentCount();
        $allNewsPosts2 = $this->blogModel2->printNews();
        return $allNewsPosts2;
    }

}
