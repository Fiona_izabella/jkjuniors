<?php
//include('application/Router.php');
include('router.php');
include('config/initialise.php');
include('config/db.php');

//create a new object of teh class so that you can utilize its methods and variables
$router = new Router();
$conn = new Db();

$conn->connectDb();
$router->add('/', array('home', 'home'));
$router->add('home', array('home', 'home'));
$router->add('about', array('About', 'about'));
$router->add('comments', array('Comments', 'comments'));
$router->add('news', array('News', 'news'));
$router->add('contact', array('Contact', 'contact'));
$router->add('test', array('Test', 'test'));
$router->add('paginationNew', array('PaginationNew', 'paginationNew'));
$router->add('newsblog', array('Newsblog', 'newsblog'));
$router->add('adminNews', array('adminNews', 'adminNews'));
$router->add('services', array('services', 'services'));
$router->add('gallery', array('Gallery', 'gallery'));
$router->add('login', array('login', 'login'));
$router->add('logout', array('Logout', 'logout'));

$router->handleRequest();