/*======= nav toggle============= */

var menuShow = false;

$(document).ready(function() {
    function checkWidth() {
        var mq;
        if (document.body.clientWidth <= 480) mq = true;
        if (mq) {

            $('.clicker').show();
            $('.menu-container ul').hide();

        } else {
            $('.clicker').hide();
            $('.menu-container ul').show();
        }

    }

    function init() {

        $('.clicker').click(function(e) {
            if (!menuShow) {
                console.log('Worked');
                $('nav .menu-container ul').slideToggle(200);
                $('.jumbotron').toggleClass('newclass');
                $('.jumbotronContact').toggleClass('newclass');
                $('.container.gallery').toggleClass('newclass');

            }

        });
    }

    init();
    checkWidth();
    // Bind event listener
    $(window).resize(checkWidth);

});


/*==== change colour of letters ==*/
$(document).ready(function() {

    $(".letters:nth-child(odd)").addClass("odd");
    $(".letters:nth-child(even)").addClass("even");
    $("span").each(function(index) {
        var originalText = $(this).text();
        var newText = "";
        for (var i = 0; i < originalText.length; i++) {
            if (i % 5 === 0)
                newText += "<span>" + originalText.charAt(i) + "</span>";
            else
                newText += originalText.charAt(i);
        }
        $(this).html(newText);
    });

});

// slide show function
var canvas, ctx;
var aImages = [];
var iCurSlide = 0;
var iCnt = 0;
var iSmTimer = 0;
var iContr = 0;
var iEfIter = 50;

$(function() {
    // creating canvas objects
    canvas = document.getElementById('slideshow');
    ctx = canvas.getContext('2d');

    // collect all images
    $('.slides').children().each(function(i) {
        var oImg = new Image();
        oImg.src = this.src;
        aImages.push(oImg);
    });

    // draw first image
    ctx.drawImage(aImages[iCurSlide], 0, 0);
    var iTimer = setInterval(changeSlideTimer, 5000); // set inner timer
});

function changeSlideTimer() {
    iCurSlide++;
    if (iCurSlide == $(aImages).length) {
        iCurSlide = 0;
    }

    clearInterval(iSmTimer);
    iSmTimer = setInterval(drawSwEffect, 40); // extra one timer
}

// draw switching effect
function drawSwEffect() {
    iCnt++;

    if (iCnt <= iEfIter / 2) {
        iContr += 0.004;
        // change brightness and contrast
        Pixastic.process(canvas, 'brightness', {
                'brightness': 2,
                'contrast': 0.0 + iContr,
                'leaveDOM': true
            },
            function(img) {
                ctx.drawImage(img, 0, 0);
            }
        );
    }

    if (iCnt > iEfIter / 2) {
        // change brightness
        Pixastic.process(canvas, 'brightness', {
                'brightness': -2,
                'contrast': 0,
                'leaveDOM': true
            },
            function(img) {
                ctx.drawImage(img, 0, 0);
            }
        );
    }

    if (iCnt == iEfIter / 2) { // switch actual image
        iContr = 0;
        ctx.drawImage(aImages[iCurSlide], 0, 0);

        Pixastic.process(canvas, 'brightness', {
                'brightness': iEfIter,
                'contrast': 0,
                'leaveDOM': true
            },
            function(img) {
                ctx.drawImage(img, 0, 0);
            }
        );
    } else if (iCnt == iEfIter) { // end of cycle, clear extra sub timer
        clearInterval(iSmTimer);
        iCnt = 0;
        iContr = 0;
    }
}

/*============================= stars ==============================*/

/*============star1 =================*/
var stage = new Kinetic.Stage({
    container: 'starContainer',
    width: 80,
    height: 80,
    fill: '#000',
    stroke: 'black',
    strokeWidth: 2

});
var layer = new Kinetic.Layer();

var star = new Kinetic.Star({
    x: 41,
    y: 40,
    numPoints: 5,
    innerRadius: 15,
    outerRadius: 40,
    fill: 'orange',
    stroke: 'black',
    strokeWidth: 2
});

var complexText = new Kinetic.Text({
    x: 30,
    y: 20,
    // text: 'i ',
    fontSize: 30,
    fontFamily: 'Arial',
    fill: '#000',
    align: 'right'

});
layer.add(star);
layer.add(complexText);
stage.add(layer);

/*============star2 =================*/
var stage2 = new Kinetic.Stage({
    container: 'starContainer2',
    width: 80,
    height: 80
});
var layer2 = new Kinetic.Layer();
var star2 = new Kinetic.Star({
    x: 41,
    y: 40,
    numPoints: 5,
    innerRadius: 15,
    outerRadius: 40,
    fill: 'orange',
    stroke: 'black',
    strokeWidth: 2
});

// add the shape to the layer
layer2.add(star2);

// add the layer to the stage
stage2.add(layer2);

/*============star3 =================*/
var stage3 = new Kinetic.Stage({
    container: 'starContainer3',
    width: 80,
    height: 80,
    fill: '#000',
    stroke: 'black',
    strokeWidth: 2

});
var layer3 = new Kinetic.Layer();

var star3 = new Kinetic.Star({
    x: 35,
    y: 40,
    numPoints: 5,
    innerRadius: 15,
    outerRadius: 40,
    fill: 'orange',
    stroke: 'black',
    strokeWidth: 2

});

var complexText3 = new Kinetic.Text({
    x: 30,
    y: 20,
    text: 'o ',
    fontSize: 30,
    fontFamily: 'Arial',

    fill: '#000',
    align: 'right'

});

// add the shape to the layer
layer3.add(star3);
layer3.add(complexText3);

// add the layer to the stage
stage3.add(layer3);
