<?php
class Router{
	protected $routes = array();


	public function add($path, $controller){
		$this->routes[$path] = $controller;

	}

	public function handleRequest(){
		$current_url = isset($_GET['url']) ? $_GET['url'] : '/';
		 if(array_key_exists($current_url, $this->routes)){
			//requested url was found
			$controller = $this->routes[$current_url];
			$cont_name = $controller[0];
			$cont_method = $controller[1];
			include('application/controllers/'.$cont_name.'.php');
			$obj = new $cont_name;
			$obj->$cont_method();

			}else{

			include('application/controllers/404.php');

		}
	} 

}