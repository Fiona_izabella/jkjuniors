<?php

include ('Router.php');
include ('config/initialise.php');
include ('config/db.php');

//sanitize post value
$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

//validate page number is really numaric
if (!is_numeric($page_number)) {die('Invalid page number!');
}

//get current starting point of records
$position = ($page_number * $item_per_page);

//Limit our results within a specified range.
$results = mysql_query("SELECT name,title,body FROM comments ORDER BY id ASC LIMIT $position, $item_per_page");

//output results from database
echo '<ul class="page_result">';
while ($row = mysql_fetch_array($results)) {
	echo '<li id="item_' . $row["id"] . '">' . $row["id"] . '. <span class="page_name">' . $row["name"] . '</span><span class="page_message">' . $row["message"] . '</span></li>';
}
echo '</ul>';
?>

